close all;clear all;clc;
%
%strain = importdata('TS_sinus_1.csv');
%strain = rmfield(strain,{'textdata','colheaders'});
fc = 1.0;
Tc = 3.0*fc;
vtm = 0:0.001:Tc;
strain.data =[vtm.',sin(2.0*fc*pi*vtm.')+1.0/4.0*sin(2.0*4.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.')];

%strain.data = [(1:1001)',(-1e-2:1e-5:0)',(-1e-2:1e-5:0)',fliplr(-1e-2:1e-5:0)'];
strain.maxa = 1e-3;
strain.data(:,2:end) = strain.maxa.*...
    strain.data(:,2:end)./max(abs(strain.data(:,2:end)),[],1);

strain.ntm = size(strain.data,1);
%
% figure
% plot(strain.data(:,1),strain.data(:,2),'b');
% hold all
% plot(strain.data(:,1),strain.data(:,3),'r');
% plot(strain.data(:,1),strain.data(:,4),'color',rgb('intensegamma_refeen'));
% leg=legend('ACC','VEL','DIS');
% set(leg,'box','off','location','northwest');
% xlabel('t [s]');
% ylabel('\gamma [1]');
% format_figures; rule_fig(gcf);

% b1 = -5.0e-6;
% a1 = b1*4;
% a2 = 0.0;
% b0 = 0.0;
% a0 = b0;
% b2 = 0.0;
mu0 = 1700*300^2;
nu  = 1.0/3.0;
K   = 2.0*mu0.*(1.0+nu)./3.0./(1.0-2.0.*nu);
%
% HE_basics_functions;
%
% bbc.eps = 1e-8:1e-6:1e-2;
% bbc.val(1) = 0;
% for i_=2:numel(bbc.eps)
%     bbc.val(i_)=bbc.val(i_-1)+...
%         (bbc.eps(i_)-bbc.eps(i_-1))*...
%         mul(mu0,[0,0,0,0,0,bbc.eps(i_-1)],[0,0,0,0,0,bbc.val(i_-1)]);
% end
%
tensor_invariants;
lp=2;
eps = [zeros(strain.ntm,5),strain.data(:,lp+1)];
sig = zeros(strain.ntm,6);

sa   = zeros(1,6);
erev = zeros(1,6);
srev = zeros(1,6);
erev1 = zeros(1,6);
srev1 = zeros(1,6);
erev2 = zeros(1,6);
srev2 = zeros(1,6);
ebb  = zeros(1,6);
sbb  = zeros(1,6);

gamma_ref = 1e-4;
tau_ult = gamma_ref*mu0;
flag.uloading=1.0;
flag.backbone=true;

% [Fbba,~]=HE_norm_bb_curve('mod','hyperbolic',...
%         'val',tns.oct(eps(1,:))./gamma_ref);
% sa = -tau_ult.*Fbba.*tns.dv(eps(1,:))./tns.oct(eps(1,:));
% sbb = -tau_ult.*Fbba.*tns.dv(eps(1,:))./tns.oct(eps(1,:));
% ebb = tns.dv(eps(1,:));
% erev = tns.dv(eps(1,:));
% srev = -tau_ult.*Fbba.*tns.dv(eps(1,:))./tns.oct(eps(1,:));

dgammaoct=0.0;
omega=0.0;
domega=0.0;
lcf = 1;
omegam=1e-15;
domegae=0;
dgammaoctm=0;
domegamax=1e-15;
domegamin=1e15;

n_loop=0; % initialize the number of loops performed

for i_=2:strain.ntm
    % current strain/strain increment
    e  = eps(i_-1,:);
    de = eps(i_,:)-e;
    
    % update number of the loop
    if (eps(i_-1,end)<=0 && eps(i_,end)>=0) && sig(i_-1,end)>=0
        n_loop=n_loop+1;
        n_enter_this_loop(n_loop)=0;
    end
    
    % backbone curve
    [Fbba,~]=HE_norm_bb_curve('mod','hyperbolic',...
        'val',tns.oct(e(:))./gamma_ref);
    [Fbb,dFbb]=HE_norm_bb_curve('mod','hyperbolic',...
        'val',tns.oct(de(:)+e(:))./gamma_ref);
    dgammaoct(i_) = tns.oct(e(:)+de(:))-tns.oct(e(:));
    dgammaoctm = max([dgammaoctm,abs(dgammaoct(i_))]);
    
    dst = HE_dstress('epsilon',e,'depsilon',de,...
        'Gs',Fbba*tau_ult/(max(1e-15,tns.oct(e(:)+de(:)))),'Gt',dFbb*mu0,'K',K);
    dse = HE_dstress('epsilon',e,'depsilon',de,...
        'Gs',mu0,'Gt',mu0,'K',K);
    domegae(i_) = complementary_energy('strain',e(:),'dstress',dse);
    domega(i_) = complementary_energy('strain',e(:),'dstress',dst);
    omega(i_)=omega(i_-1)+domega(i_);
    lcf(i_) = URL_conditions(e(:),dst(:),omega(i_-1),omegam);
    if lcf(i_)==4
        lcf(i_)=lcf(i_-1);
    end
    omegam=max(omegam,omega(i_));
    domegamax=max(domegamax,domegae(i_));
    domegamin=min(domegamin,domegae(i_));
    
    if sign(domegae(i_))*sign(domegae(i_-1))<0
        erev2=erev1;
        srev2=srev1;
        erev1=erev;
        srev1=srev;
        erev=e;
        srev=sa;
        flag.backbone=false;
    end
    
        
    if round(tns.oct(e)*1e12)>=round(tns.oct(ebb)*1e12)
        ebb=e;
        sbb=sa;
    end
    
    % if i_>=700
    %     keyboard;
        % figure(1)
	% plot(eps(1:i_-1,end),sig(1:i_-1,end),'b'); hold all; grid on
    % plot(eps(1001,end),sig(1001,end),'x');
    
    % end
    
    if lcf(i_)==1
        disp('LOADING')
        dsa = HE_dstress('epsilon',e,'depsilon',de,...
            'Gs',Fbb*tau_ult/tns.oct(e(:)+de(:)),'Gt',dFbb*mu0,'K',K);
        sa = sa(:)+dsa;
        sbb=sa;
        ebb=e+de;
    elseif lcf(i_)==2
        disp('UNLOADING')
        [phi,dphi] = HE_hysteresis_functional('mod','puzrin_burland_1996',...
            'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(ebb),...
            'gamma_val',tns.oct(e+de-erev),'tau_bb',tns.oct(sbb),...
            'tau_ult',tau_ult);
        dsa = HE_dstress('epsilon',e,'depsilon',de,'Gs',phi,'Gt',dphi,'K',K);
        sa = sa+dsa;
    elseif lcf(i_)==3
        disp('RE-LOADING')
        [phi,dphi] = HE_hysteresis_functional('mod','puzrin_burland_1996',...
            'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(ebb),...
            'gamma_val',tns.oct(e+de-erev),'tau_bb',tns.oct(sbb),...
            'tau_ult',tau_ult);
        dsa = HE_dstress('epsilon',e,'depsilon',de,'Gs',phi,'Gt',dphi,'K',K);
        sa = sa+dsa;
    end
    
    if flag.backbone==false
        flag.backbone = round(tns.oct(sa)*1e12)>round(tns.oct(sbb)*1e12)&&...
            sign(tns.oct(e)-tns.oct(erev1))*sign(tns.oct(e(:)+de(:))-tns.oct(erev1))<0;
        if flag.backbone
            dsa = HE_dstress('epsilon',e,'depsilon',de,...
                'Gs',Fbb*tau_ult/tns.oct(e(:)+de(:)),'Gt',dFbb*mu0,'K',K);
            sa = sig(i_-1,:).'+dsa;
            sbb=sa;
            ebb=e+de;
        else
            if i_>1169
              %  keyboard
		% plot(eps(1:i_-1,end),sig(1:i_-1,end),'b'); hold all;
              
            end
%             flag.envelope = round(tns.oct(sa)*1e12)>...
%                 round(tns.oct(senv)*1e12) && ...
%                 sign(tns.oct(e)-tns.oct(ebb))*sign(tns.oct(e(:)+de(:))-tns.oct(ebb))<0;
            flag.envelope = sign(tns.oct(sa)-tns.oct(srev1))*sign(tns.oct(sig(i_-1,:))-tns.oct(srev1))<0 && ...
                sign(tns.oct(e)-tns.oct(erev1))*sign(tns.oct(e(:)+de(:))-tns.oct(erev1))<0;
            if flag.envelope 
                sa=srev1(:);
                de=de-erev1+e;
                [phi_env,dphi_env] = HE_hysteresis_functional('mod','puzrin_burland_1996',...
                    'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(srev2),...
                    'gamma_val',tns.oct(erev1(:)+de(:)-erev2(:)),...
                    'tau_bb',tns.oct(srev2),'tau_ult',tau_ult);
                dss = HE_dstress('epsilon',erev1(:),'depsilon',de(:),'Gs',phi_env,...
                    'Gt',dphi_env,'K',K);
                sa = srev1(:)+dss(:);
                erev=erev2;
                srev=srev2;
                erev1=erev2;
                srev1=srev2;

            elseif flag.envelope==false && ...
                    sig(i_-1,end)>=0 && eps(i_-1,end)>=0 &&...
                    lcf(i_)==3 &&...
                    any(sig(1:i_-2,end)>=tau_ult) 
                
                n_enter_this_loop(n_loop)=n_enter_this_loop(n_loop)+1;
                
                if n_enter_this_loop(n_loop)==1 || entered_this_loop==1
                    
                    sa=sa+(sig(i_-1,:)-sig(i_-2,:))'-dsa;
                    entered_this_loop=1;
                end
                   
            elseif flag.envelope==false && ...
                    sig(i_-1,end)>=0  &&...
                    abs(eps(i_-1,end))<=10^-15 &&...
                    lcf(i_)==2 &&...
                    any(sig(1:i_-2,end)>=tau_ult) 
                
                sa=sa+(sig(i_-1,:)-sig(i_-2,:))'-(2.*dsa);
                
%                 sa=-srev1(:);
%                 de=de-erev1+e;
%                 [phi_env,dphi_env] = HE_hysteresis_functional('mod','puzrin_burland_1996',...
%                     'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(srev2),...
%                     'gamma_val',tns.oct(erev1(:)+de(:)-erev2(:)),...
%                     'tau_bb',tns.oct(srev2),'tau_ult',tau_ult);
%                 dss = HE_dstress('epsilon',erev1(:),'depsilon',de(:),'Gs',phi_env,...
%                     'Gt',dphi_env,'K',K);
%                 sa = -srev1(:)+dss(:);
%                 erev=erev2;
%                 srev=srev2;
%                 erev1=erev2;
%                 srev1=srev2;
            else
                    entered_this_loop=0;
            end
        end
    end
    
    if sign(tns.oct(sa)-tns.oct(srev1))*sign(tns.oct(sig(i_-1,:))-tns.oct(srev1))<0 &&...
        sign(tns.oct(e(:)+de(:))-tns.oct(erev1))*sign(tns.oct(e)-tns.oct(erev1))<0
        erev2=erev1;
        srev2=srev1;
        erev1=erev;
        srev1=srev;
    end
        
    sig(i_,:) = sa(:).';
    
    % radius and angle of the envelop at the i-step
    
    rad(i_)=sqrt((sa(end)./tau_ult)^2+((e(end)+de(end))./gamma_ref)^2);
    
    ang(i_)=atan((sa(end)./tau_ult)/((e(end)+de(end))./gamma_ref));
    
end

figure
plot(eps(:,end),sig(:,end)./1e6,'b');
xlabel('\gamma [1]');
ylabel('\tau [MPa]');
% format_figures; rule_fig(gcf);

figure(2)
    plot(1:i_,rad) % inserito 'a mano' quello attuale
    title('radius')
    
figure(3)
    plot(1:i_,ang) % inserito 'a mano' quello attuale
    title('angle')

% figure
% semilogx(eps(:,end),sig(:,end)./eps(:,end)./mu0,'b');
% xlabel('\gamma [1]');
% ylabel('\mu/\mu_0 [1]');
% format_figures; rule_fig(gcf);
