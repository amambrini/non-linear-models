function [ebb_plot,sbb_plot]=plot_bb(strain,eps,tns,mu0,K,tau_ult,gamma_ref)

sbb_plot = zeros(strain.ntm,6);
ebb_plot = zeros(strain.ntm,6);
for i_bb=2:strain.ntm
    de = eps(i_bb,:)-eps(i_bb-1,:);    
    % backbone curve
%     [Fbba,~]=HE_norm_bb_curve('mod','hyperbolic',...
%         'val',tns.oct(eps(i_bb-1,:)')./gamma_ref);
    [Fbb,dFbb]=HE_norm_bb_curve('mod','hyperbolic',...
        'val',tns.oct(de(:)+eps(i_bb-1,:)')./gamma_ref);
    
    dsa = HE_dstress('epsilon',eps(i_bb-1,:),'depsilon',de,...
        'Gs',Fbb*tau_ult/tns.oct(eps(i_bb-1,:)'+de(:)),'Gt',dFbb*mu0,'K',K);
    sbb_plot(i_bb,:)=sbb_plot(i_bb-1,:)+dsa';
    ebb_plot(i_bb,:)=eps(i_bb-1,:)+de;
end
end