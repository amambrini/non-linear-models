ccc;
global Gmax taub taup taudu gamma0
Gmax = 1e6;
taub = -20;
taup = 30;
taudu = -50;
gamma0=1e-5;

fun=@underloop;
x0=[10,10,200];
x=fsolve(fun,x0);