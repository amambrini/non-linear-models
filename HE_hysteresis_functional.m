function [varargout]=HE_hysteresis_functional(varargin)
    % HE_HYSTERESIS_FUNCTIONAL  Compute different hysteresis backbone curves
    %
    %   PHI = HYSTERESIS_FUNCTIONAL('mod',MODEL,'Gmax',GMAX,'gamma_val',GAMMAVAL,...
    %       'gamma_bb',GAMMABB,'tau_bb',TAUBB,'tau_ult',TAU_ULT) 
    %
    % Compute the MODEL hysteretic curve for shear strain value GAMMAVAL with 
    %       model parameters ALPHA and R, defined by Muravskii (2005) and
    %       based on the maximum shear modulus GMAX, the last values of
    %       (GAMMABB,TAUBB) on the backbone curve and the ultimate value of
    %       the shear stress TAU_ULT. The increment DPHI is computed as
    %       well for incremental formulation
    % MODEL available
    %   'puzrin_burland_1996': 
    %
    %       PHI(GAMMA)=GAMMA*(1-ALPHA*(log(1+abs(gamma)))^R)
    %
    %   'davidenkov_1938':
    %
    %       PHI(GAMMA)=GAMMA*(1-ALPHA*abs(gamma)^R)
    %
    % REFERENCES
    % # Muravskii, G. 2005. On description of hysteretic behaviour of materials.
    %   International Journal of Solids and Structures, 42, 2625-2644
    % # Puzrin, A.M., Burland, J.B., 1996. A logarithmic stress???strain function 
    %   for rocks and soils. Geotechnique 46 (1), 157-164.
    % # Davidenkov, N.N., 1938. Energy dissipation in vibrations. 
    %   Journal of Technical Physics 8 (6) (in Russian).
    % # Pyke, R., 1979. Nonlinear soil model for irregular cyclic loadings.
    %   Journal of the Geotechnical Engineering Division, ASCE 105, 715-726.
    
    %% *DEFAULT*
    def.mod = 'puzrin_burland_1996';
    def.bbmod = 'hyperbolic';
    def.gamma_val= 0.0e-4;
    def.gamma_bb = 0.0e-3;
    def.tau_bb = 0.0e4;
    def.Gmax = 0.0e6;
    def.tau_ult = 0.0e4;
    
    %% *PARSE PARAMETERS*
    inp = inputParser;
    addParameter(inp,'mod',def.mod,@ischar);
    addParameter(inp,'bbmod',def.bbmod,@ischar);
    addParameter(inp,'Gmax',def.Gmax,@isnumeric);
    addParameter(inp,'gamma_val',def.gamma_val,@isnumeric);
    addParameter(inp,'gamma_bb',def.gamma_bb,@isnumeric);
    addParameter(inp,'tau_bb',def.tau_bb,@isnumeric);
    addParameter(inp,'tau_ult',def.tau_ult,@isnumeric);
    parse(inp,varargin{:});
    
    %% *PARSE PARAMETERS*
    mod       = inp.Results.mod;
    Gmax      = inp.Results.Gmax;
    gamma.val = inp.Results.gamma_val;
    gamma.bb  = inp.Results.gamma_bb;
    tau.bb    = inp.Results.tau_bb;
    tau.ult   = inp.Results.tau_ult;
    gamma.ref = tau.ult./Gmax;
    
    %% *NORMALIZATION*
    xx = @(strain) abs(strain)./gamma.ref;
    yy = @(stress) stress./tau.ult;
    %
    [~,dFbb] = HE_norm_bb_curve('mod',inp.Results.bbmod,...
        'val',xx(gamma.bb));
    %    
    switch lower(mod)
        case 'puzrin_burland_1996'
            %
            R     = (yy(tau.bb)-xx(gamma.bb).*dFbb).*...
                (1.0+2.0.*xx(gamma.bb)).*...
                log(1.0+2.0.*xx(gamma.bb))./2.0./xx(gamma.bb)./...
                (xx(gamma.bb)-yy(tau.bb));
            %
            alpha = (xx(gamma.bb)-yy(tau.bb))./xx(gamma.bb)./...
                ((log(1.0+2.0.*xx(gamma.bb))).^R);
            %
            phi  = @(u) u.*(1.0-alpha.*(log(1.0+abs(u))).^R);
            dphi = @(u) (1.0-alpha.*(log(1.0+abs(u))).^R)-...
                abs(u).*alpha.*R.*(log(1.0+abs(u)).^(R-1.0))./(1.0+abs(u));
        case 'davidenkov_1938'
            %
            R     = (yy(tau.bb)-xx(gamma.bb).*dFbb)./(xx(gamma.bb)-yy(tau.bb));
            %
            alpha = (xx(gamma.bb)-yy(tau.bb))./xx(gamma.bb)./...
                ((2.0.*xx(gamma.bb)).^R);
            %
            phi  = @(u) u.*(1.0-alpha.*abs(u).^R);
            dphi = @(u) (1.0-alpha.*abs(u).^R)-...
                abs(u).*alpha.*R.(abs(u).^(R-1.0));
    end
    
    %% *OUTPUT*
    varargout{1} = tau.ult.*phi(gamma.val./gamma.ref);
    varargout{2} = tau.ult.*dphi(gamma.val./gamma.ref)./gamma.ref;
    
    return
end