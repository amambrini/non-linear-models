function [varargout] = HE_dstress(varargin)
    tensor_invariants;
    def.depsilon = zeros(6,1);
    def.epsilon = zeros(6,1);
    def.Kt = 0.0;
    def.Gs = 0.0;
    def.Gt = 0.0;
    %
    inp = inputParser;
    addParameter(inp,'depsilon',def.depsilon,@isnumeric);
    addParameter(inp,'epsilon',def.epsilon,@isnumeric);
    addParameter(inp,'Kt',def.Kt,@isnumeric);
    addParameter(inp,'Gs',def.Gs,@isnumeric);
    addParameter(inp,'Gt',def.Gt,@isnumeric);
    %
    parse(inp,varargin{:});
    %
    FLD=inp.Results;
    STR=structvars(FLD);
    for i_=1:size(STR,1)
        eval(STR(i_,:));
    end
    %
    depsilon = tns.Am1*depsilon(:);
    epsilon = tns.Am1*epsilon(:);
    e   = tns.dv(epsilon);
    eta = 2.0./3.0.*(Gt-Gs)/max(1e-15,(tns.oct(epsilon(:)).^2.0));
    dsigma(:) = 2.0.*((Kt./2.0-Gs./3.0)*tns.mv(:)*tns.mv(:).'+...
        Gs*eye(6)+eta.*e(:)*e(:).')*depsilon(:);
    varargout{1} = dsigma(:);
    return
end