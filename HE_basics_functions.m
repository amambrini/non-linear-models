tensor_invariants;
LTOL =1e-6;
cl   = @(x1,x2) tns.dv(x1).'*tns.dv(x2)./...
    max(LTOL,(norm(tns.dv(x1),2).*norm(tns.dv(x2),2)));

xi   = @(xr,xs) tns.oct(xr(:)-xs(:));
g1   = @(xe,xs) min(a0+a1.*log(tns.oct(xe))+a2.*tns.pr(xs),1e20);
g2   = @(xe,xs) min(b0+b1.*log(tns.oct(xe))+b2.*tns.pr(xs),1e20);
mul  = @(xm,xe,xs)    xm./(1.0+g1(xe,xs)*tns.oct(xs));
muu  = @(xm,xe,xr,xs) xm./(1.0+g2(xe,xs)*xi(xr,xs));