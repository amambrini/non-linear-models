% constants
tns.Am = eye(6);
tns.Am(4:6,4:6) = 2.0*eye(3);
tns.Am1 = eye(6);
tns.Am1(4:6,4:6) = 0.5*eye(3);
tns.mv   = [1.0,1.0,1.0,0.0,0.0,0.0].';
% tensor invariants
tns.I1   = @(x1) tns.mv(:).'*x1(:);
tns.I2   = @(x1) x1(1)*x1(2)+x1(2)*x1(3)+x1(3)*x1(1)-...
    (x1(4)^2.0+x1(5)^2.0+x1(6)^2.0);
tns.I3   = @(x1) prod(x1(1:3))+2.0*prod(x1(4:6))-sum(x1(4:6).^2.0.*x1(3:-1:1));
% tensor components
tns.pr   = @(x1) (1.0./3.0).*tns.I1(x1);
tns.dv   = @(x1) x1(:)-tns.pr(x1).*tns.mv(:);
% deviatoric tensor invariants
tns.J2   = @(x1) 0.5*tns.dv(x1).'*tns.Am*tns.dv(x1);
tns.J2p  = @(x1p) (1.0./6.0.*((x1p(1)-x1p(2)).^2.0+(x1p(2)-x1p(3)).^2.0+...
    (x1p(1)-x1p(3)).^2.0));
tns.J3   = @(x1) (2.0./27.0)*(tns.I1(x1).^3.0)-(1.0./3.0*tns.I1(x1).*tns.I2(x1))+tns.I3(x1);
% tensor principal components
tns.lac = @(x1) (1.0./3.0*acosd(0.5.*tns.J3(x1).*(3.0./tns.J2(x1)).^1.5));
tns.las = @(x1) 30.0-tns.lac(x1);
tns.pcr  = @(x1) tns.pr(x1).*ones(3,1)+...
    sqrt(4.0./3.0*tns.J2(x1)).*...
    [cosd(tns.lac(x1));cosd(tns.lac(x1)-120.0);cosd(tns.lac(x1)+120.0)];
% octhaedral invariants
tns.oct  = @(x1) sqrt(2.0/3.0*tns.J2(x1));
tns.octp = @(x1) sqrt(2.0/3.0*tns.J2p(x1));

% Lode coordinates
tns.v2t = @(x1) [x1(1),x1(4),x1(5);x1(4),x1(2),x1(6);x1(5),x1(6),x1(3)];
tns.t2v = @(x1) [x1(1,1);x1(2,2);x1(3,3);x1(1,2);x1(1,3);x1(2,3)];
tns.tra = @(x1) x1(1,1)+x1(2,2)+x1(3,3);
tns.cnt = @(x1,x2) tns.tra(x1.'*x2);
tns.dvt = @(x1) tns.v2t(tns.dv(x1));
tns.Ez = 1.0./sqrt(3.0).*eye(3);
tns.Er = @(x1) tns.dvt(x1)./sqrt(2.0.*tns.J2(x1));
tns.T  = @(x1) tns.dvt(x1)*tns.dvt(x1)-2.0*tns.J2(x1)./3.0.*eye(3);
tns.Ets = @(x1) (tns.T(x1)./sqrt(2.0*tns.J2(tns.t2v(tns.T(x1))))-...
    sind(3.0*tns.las(x1))*tns.Ez)./cosd(3.0.*tns.las(x1));
tns.Etc = @(x1) (tns.T(x1)./sqrt(2.0*tns.J2(tns.t2v(tns.T(x1))))-...
    cosd(3.0*tns.lac(x1))*tns.Ez)./sind(3.0.*tns.las(x1));