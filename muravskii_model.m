ccc;
%
%strain = importdata('TS_sinus_1.csv');
%strain = rmfield(strain,{'textdata','colheaders'});
fc = 1.0;
Tc = 3.0*fc;
vtm = 0:0.001:Tc;
strain.data =[vtm.',sin(2.0*fc*pi*vtm.')+1.0/4.0*sin(2.0*4.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.')];

%strain.data = [(1:1001)',(-1e-2:1e-5:0)',(-1e-2:1e-5:0)',fliplr(-1e-2:1e-5:0)'];
strain.maxa = 1e-3;
strain.data(:,2:end) = strain.maxa.*...
    strain.data(:,2:end)./max(abs(strain.data(:,2:end)),[],1);

strain.ntm = size(strain.data,1);
%
% figure
% plot(strain.data(:,1),strain.data(:,2),'b');
% hold all
% plot(strain.data(:,1),strain.data(:,3),'r');
% plot(strain.data(:,1),strain.data(:,4),'color',rgb('intensegamma_refeen'));
% leg=legend('ACC','VEL','DIS');
% set(leg,'box','off','location','northwest');
% xlabel('t [s]');
% ylabel('\gamma [1]');
% format_figures; rule_fig(gcf);

% b1 = -5.0e-6;
% a1 = b1*4;
% a2 = 0.0;
% b0 = 0.0;
% a0 = b0;
% b2 = 0.0;
mu0 = 1700*300^2;
nu  = 1.0/3.0;
K   = 2.0*mu0.*(1.0+nu)./3.0./(1.0-2.0.*nu);
%
% HE_basics_functions;
%
% bbc.eps = 1e-8:1e-6:1e-2;
% bbc.val(1) = 0;
% for i_=2:numel(bbc.eps)
%     bbc.val(i_)=bbc.val(i_-1)+...
%         (bbc.eps(i_)-bbc.eps(i_-1))*...
%         mul(mu0,[0,0,0,0,0,bbc.eps(i_-1)],[0,0,0,0,0,bbc.val(i_-1)]);
% end
%
tensor_invariants;
lp=3;
eps = [zeros(strain.ntm,5),strain.data(:,lp+1)];
sig = zeros(strain.ntm,6);

sa   = zeros(1,6);
erev = zeros(1,6);
srev = zeros(1,6);
ebb  = zeros(1,6);
sbb  = zeros(1,6);

gamma_ref = 1e-4;
tau_ult = gamma_ref*mu0;
flag.uloading=1.0;
flag.backbone=true;

% [Fbba,~]=normalized_backbone_curve('mod','hyperbolic',...
%         'val',tns.oct(eps(1,:))./gamma_ref);
% sa = -tau_ult.*Fbba.*tns.dv(eps(1,:))./tns.oct(eps(1,:));
% sbb = -tau_ult.*Fbba.*tns.dv(eps(1,:))./tns.oct(eps(1,:));
% ebb = tns.dv(eps(1,:));
% erev = tns.dv(eps(1,:));
% srev = -tau_ult.*Fbba.*tns.dv(eps(1,:))./tns.oct(eps(1,:));

oct.gamma=0.0;
oct.tau=0.0;

for i_=1:strain.ntm-1
    % current strain/strain increment
    e  = eps(i_,:);
    de = eps(i_+1,:)-e;
    % backbone curve
    [Fbba,~]=normalized_backbone_curve('mod','hyperbolic',...
        'val',tns.oct(e(:))./gamma_ref);
    [Fbb,~]=normalized_backbone_curve('mod','hyperbolic',...
        'val',tns.oct(de(:)+e(:))./gamma_ref);
    flag.uloading1 = (1.0./max(1e-6,sqrt(2.0*tns.J2(e)))).*e*de.';
    
    
    if oct.gamma(i_)<=0
        if flag.backbone
            disp('case 1')
            sa = tns.I1(e(:)+de(:)).*K*tns.mv+...
                tau_ult.*Fbb*tns.dv(de(:)+e(:))./max(1e-15,tns.oct(de(:)+e(:)));
            sbb=sa;
            ebb=e+de;
        else
            disp('case 2')
            if sign(flag.uloading1)*sign(flag.uloading)<0
                erev=e;
                srev=sa;
            end
            flag.uloading=flag.uloading1;
            phi = hysteresis_functional('mod','puzrin_burland_1996',...
                'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(ebb),...
                'gamma_val',tns.oct(e+de)-tns.oct(erev),'tau_bb',tns.oct(sbb),...
                'tau_ult',tau_ult);
            sa = tns.I1(e(:)+de(:)).*K*tns.mv+...
                tns.dv(srev)+...
                phi*(tns.dv(erev)./max(1e-15,tns.oct(erev)));
        end
        
    else
        if flag.backbone
            disp('case 3')
            sa = tns.I1(e(:)+de(:)).*K*tns.mv+...
                tau_ult.*Fbb*tns.dv(e(:)+de(:))./max(1e-15,tns.oct(e(:)+de(:)));
            sbb=sa;
            ebb=e+de;
        else
            disp('case 4')
            if sign(flag.uloading1)*sign(flag.uloading)<0
                erev=e;
                srev=sa;
            end
            flag.uloading=flag.uloading1;
            phi = hysteresis_functional('mod','puzrin_burland_1996',...
                'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(ebb),...
                'gamma_val',tns.oct(e+de)-tns.oct(erev),'tau_bb',tns.oct(sbb),...
                'tau_ult',tau_ult);
            
            sa = tns.I1(e(:)+de(:)).*K*tns.mv+...
                tns.dv(srev)+...
                phi*tns.dv(e(:)+de(:))./max(1e-15,tns.oct(e(:)+de(:)));
        end
    end
    
    flag.backbone = round(tns.oct(sa)*1e12)>round(tau_ult.*Fbb*1e12);
    
    if flag.backbone
        if oct.gamma(i_)>0 
            sa = tns.I1(e(:)+de(:)).*K*tns.mv+...
                tau_ult.*Fbb*tns.dv(e(:)+de(:))./max(1e-15,tns.oct(e(:)+de(:)));
            sbb=sa;
            ebb=e+de;
        else
            flag.backbone=false;
        end
    end
    sig(i_+1,:) = sa(:).';
    
    oct.gamma(i_+1,1) = tns.oct(e(:)+de(:))-tns.oct(e(:));
end

figure
plot(eps(:,end),sig(:,end)./1e6,'b');
xlabel('\gamma [1]');
ylabel('\tau [MPa]');
format_figures; rule_fig(gcf);

% figure
% semilogx(eps(:,end),sig(:,end)./eps(:,end)./mu0,'b');
% xlabel('\gamma [1]');
% ylabel('\mu/\mu_0 [1]');
% format_figures; rule_fig(gcf);
