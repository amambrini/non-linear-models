function [varargout]=compute_envelope(varargin)
    ebb=varargin{1};
    sbb=varargin{2};
    tau_ult=varargin{3};
    mu0=varargin{4};
    K = varargin{5};
    
    tensor_invariants;
    
    idx=tns.oct(ebb):-1e-6:-tns.oct(ebb);
    idx=numel(idx);
    for ii_=1:idx
        envg(ii_,:) = ebb(:).'+(ii_-1)*1e-6*[0,0,0,0,0,1];
    end
    ss=sbb(:).';
    for ii_=2:size(envg,1)
        de = envg(ii_,:)-envg(ii_-1,:);
        [phii(ii_),dphii(ii_)] = HE_hysteresis_functional('mod','puzrin_burland_1996',...
            'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(ebb),...
            'gamma_val',tns.oct(envg(ii_,:)-ebb(:).'),...
            'tau_bb',tns.oct(sbb),...
            'tau_ult',tau_ult);
        dsa = HE_dstress('epsilon',envg(ii_-1,:),'depsilon',de,'Gs',phii(ii_),'Gt',dphii(ii_),'K',K);
        ss(ii_,:) = ss(ii_-1,:)+dsa(:).';
    end
    %plot(envg(:,end),ss(:,end))
    varargout{1}=ss(end,:);
    return
end

