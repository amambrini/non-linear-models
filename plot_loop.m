function [e_loop,s_loop]=plot_loop(strain,eps,tns,mu0,K,tau_ult,gamma_ref)

e_loop = zeros(strain.ntm,6);
s_loop = zeros(strain.ntm,6);
for i_bb=2:strain.ntm
    de = eps(i_bb,:)-eps(i_bb-1,:);    
    % backbone curve
%     [Fbba,~]=HE_norm_bb_curve('mod','hyperbolic',...
%         'val',tns.oct(eps(i_bb-1,:)')./gamma_ref);
    [Fbb,dFbb]=HE_norm_bb_curve('mod','hyperbolic',...
        'val',tns.oct(de(:)+eps(i_bb-1,:)')./gamma_ref);
    
    dsa = HE_dstress('epsilon',eps(i_bb-1,:),'depsilon',de,...
        'Gs',Fbb*tau_ult/tns.oct(eps(i_bb-1,:)'+de(:)),'Gt',dFbb*mu0,'K',K);
    sbb_plot(i_bb,:)=sbb_plot(i_bb-1,:)+dsa';
    ebb_plot(i_bb,:)=eps(i_bb-1,:)+de;
end
end

[phi,dphi] = HE_hysteresis_functional('mod','puzrin_burland_1996',...
                'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(ebb),...
                'gamma_val',tns.oct(e+de-gamma_start_unl),'tau_bb',tns.oct(sbb),...
                'tau_ult',tau_ult);
            dsa = HE_dstress('epsilon',e,'depsilon',de,'Gs',phi,'Gt',dphi,'K',K);
            sa = sa+dsa;













end