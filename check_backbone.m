function [offset,bbce,idx]=check_backbone(bbc,xe,xs)
    tensor_invariants;
    if isempty(find(tns.oct(xe)>bbc.eps,1,'last'))
        idx(1)=1;
    else
        idx(1) = find(tns.oct(xe)>bbc.eps,1,'last');
    end
    idx(2) = idx(1)+1;
    bbce = interp1(bbc.eps(idx(1:2)),bbc.val(idx(1:2)),tns.oct(xe));
    
    offset = abs(bbce-tns.oct(pcr(xs)))./tns.oct(pcr(xs));
    return
end