function [varargout]=HE_norm_bb_curve(varargin)
    def.mod = 'hyperbolic';
    def.val = 1;
    inp = inputParser;
    addParameter(inp,'mod',def.mod,@ischar);
    addParameter(inp,'val',def.val,@isnumeric);
    
    parse(inp,varargin{:});
    
    switch lower(inp.Results.mod)
        case 'hyperbolic'
            varargout{1} = inp.Results.val./(1.0+abs(inp.Results.val));
            varargout{2} = 1.0./((1.0+inp.Results.val).^2);
    end
    return
end