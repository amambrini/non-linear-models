close all; 
clear all; 
clc;
%
% strain = importdata('TS_sinus_2.csv');
% strain = rmfield(strain,{'textdata','colheaders'});
fc = 1.0;
Tc = 3.0*fc;
vtm = 0:0.0001:Tc;
strain.data =[vtm.',-sin(2.0*fc*pi*vtm.')+1.0/4.0*sin(2.0*4.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.')];

% strain.data = [(1:1001)',(0:1e-6:1e-3)',(0:1e-6:1e-3)',(0:1e-6:1e-3)'];
strain.maxa = 1e-2;
strain.data(:,2:end) = strain.maxa.*...
    strain.data(:,2:end)./max(abs(strain.data(:,2:end)),[],1);

strain.ntm = size(strain.data,1);
%
% figure
% plot(strain.data(:,1),strain.data(:,2),'b');
% hold all
% plot(strain.data(:,1),strain.data(:,3),'r');
% plot(strain.data(:,1),strain.data(:,4),'color',rgb('intensegreen'));
% leg=legend('ACC','VEL','DIS');
% set(leg,'box','off','location','northwest');
% xlabel('t [s]');
% ylabel('\gamma [1]');
% format_figures; rule_fig(gcf);

b1 = -5.0e-6;
a1 = b1*4;
a2 = 0.0;
b0 = 0.0;
a0 = b0;
b2 = 0.0;
mu0 = 2000*500^2;
nu  = 0.3;
K   = 2.0*mu0.*(1.0+nu)./3.0./(1.0-2.0.*nu);
%
HE_basics_functions;
%
bbc.eps = 1e-8:1e-6:1e-2;
bbc.val(1) = 0;
for i_=2:numel(bbc.eps)
    bbc.val(i_)=bbc.val(i_-1)+...
        (bbc.eps(i_)-bbc.eps(i_-1))*...
        mul(mu0,[0,0,0,0,0,bbc.eps(i_-1)],[0,0,0,0,0,bbc.val(i_-1)]);
end
%
lp = 1;
eps = [zeros(strain.ntm,5),strain.data(:,lp+1)];
sig = zeros(strain.ntm,6);
rev = zeros(strain.ntm,6);
sa  = zeros(1,6);
srl  = zeros(1,6);


srm = zeros(6,1);
srl = zeros(6,1);
src = zeros(6,1);
erm = zeros(6,1);
omegam = 0;
mu = mu0;

for i_=1:strain.ntm-1
    e  = eps(i_,:);
    de = -e+eps(i_+1,:);
    
    dst = mu.*tns.dv(de)+tns.I1(de).*K*tns.mv;
    [lcf,~]=URL_conditions(e,sa,dst,omegam);
    
    switch upper(lcf)
        case 'L'
            % loading
            disp('LOADING');
            mu = mul(mu0,e,sa);
        case 'U'
            % unloading
            disp('UNLOADING');
            if tns.J2(e+de)>=tns.J2(erm)
                erm = e+de;
                srm = sa;
                mu = muu(mu0,e,srm,sa);
            else
                if tns.J2(sa)>=tns.J2(src)
                    src=sa;
                end
                mu = muu(mu0,e,src,sa);
            end
            
            if tns.J2(sa)>tns.J2(srl)
                srl=zeros(6,1);
            end
        case 'R'
            % reloading
            disp('RELOADING');
            if tns.J2(sa)>tns.J2(srl)
                srl = sa;
            end
            mu = muu(mu0,e,srl,sa);
    end
    sa = sa(:)+2.0*mu.*tns.Am1*tns.dv(de)+tns.I1(de).*K*tns.mv;
    [omega,~] = ...
        complementary_energy('strain',e+de,'stress',sa);
    if omega>omegam
        omegam=omega;
    end
    
    
    %     if cl(dea,de)<=1e-16
    %         lcf=sign(eps(i_-1,:)*de(:));
    %         srl = sa;
    %         if tns.J2(srl)>tns.J2(srm1)
    %             srm0(:) = srm1(:);
    %             srm1(:) = srl(:);
    %         end
    %     end
    %     dea = de;
    %
    %     if lcf>=0
    %         if abs(tns.J2(srm1)-tns.J2(srl))<1e2
    %             mu = muu(mu0,e,srm0,sa);
    %         else
    %             mu = muu(mu0,e,srl,sa);
    %         end
    %     else
    %         if tns.J2(srl)
    %         mu = muu(mu0,e,srl,sa);
    %     end
    %     [offset,bbce,idx]=check_backbone(bbc,e,sa);
    
    sig(i_,:) = sa(:).';
    
end
figure
plot(eps(:,end),sig(:,end)./1e6,'b');
xlabel('\gamma [1]');
ylabel('\tau [MPa]');
format_figures; rule_fig(gcf);

% figure
% semilogx(eps(:,end),sig(:,end)./eps(:,end)./mu0,'b');
% xlabel('\gamma [1]');
% ylabel('\mu/\mu_0 [1]');
% format_figures; rule_fig(gcf);
