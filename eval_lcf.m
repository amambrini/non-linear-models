function [lcf]=eval_lcf(e,de,gamma_max,gamma_start_unl)
%%%%%%%%%%%%%%%%%%%%
%%%%% eval_lcf %%%%%
%%%%%%%%%%%%%%%%%%%%
% evaluate the actual condition, basing on the value of the actual strain 
% and its increment
%
% loading on bb curve
if e(end)>=0 && de(end)>0 && abs(e(end))>=gamma_max
    lcf=1;
% unloading
elseif e(end)>0 && de(end)<0
    % inside the domain
    if abs(gamma_start_unl)<gamma_max
        lcf=2.1;
    % on the border
    else
        lcf=2.2;
    end
% reloading
elseif e(end)>0 && de(end)>0 && abs(e(end))<gamma_max
    lcf=3;
end
end