function [varargout]=URL_conditions(varargin)
    strain  = varargin{1};
    dstress = varargin{2};
    omega   = varargin{3};
    omegam  = varargin{4};
    LTOL=1.0e-6;
    FTOL=1.0e-6;
    domega = complementary_energy('strain',strain,'dstress',dstress);
    
    if (abs(omega-omegam) <= +FTOL) && (domega > +LTOL)
        % loading
        lcf = 1;
    elseif (omega-omegam <=  +FTOL) && (domega < -LTOL)
        % unloading
        lcf = 2;
    elseif (omega-omegam <   -FTOL) && (domega > +LTOL)
        % reloading
        lcf = 3;
    elseif abs(domega) <= +LTOL
        lcf = 4;
    end
    
    %% OUTPUT
    try
    varargout{1} = lcf;
    catch
        keyboard
    end
    
    return
end