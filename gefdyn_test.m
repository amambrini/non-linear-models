% Uncertainty and sensitivity analysis of laboratory test simulations using an
% elastoplastic model
% Fernando Lopez-Caballero & Arezou Modaressi-Farahmand-Razavi
% Laboratoire MSS-Mat CNRS UMR 8579, Ecole Centrale Paris, France
ccc;
% test yield locus
p=0:0.01:1; % normalized pressure
b=(0:1e-3:1)'; % b-parameter
F=1-b*log(p); % F-coefficient
% test isotropic hardening
evp = 10.^(-7:.005:-3); % volumetric plastic strain
beta = [0;10.^(0:.01:2)']; % plastic volumetric compression
pc = exp(beta*evp); % normalized pressure
% degree of mobilized friction
lambda = 10.^(-6:0.005:-1); % plastic multiplier
rel = 0.03;
rhys = 0.04;
rmob = 0.8;
m = 1.5;
a1 = 0.0001;
a2 = 0.005;
alpha = @(x) 0.0*(x>=rel & x<=rhys)+...
    (((x-rhys)/(rmob-rhys)).^m)*(x>rhys & x<=rmob)+1.0*(x>rmob);
a = @(x) a1+(a2-a1)*alpha(x);
r(1) = 0;
i_=2;

for i_=2:numel(lambda)
    r(i_) = r(i_-1)+lambda(i_)*(1.0-r(i_-1))^2/a(r(i_-1));
end


% col=[hsv(numel(b));0,0,0;0,0,0];
% set(0,'defaultaxescolororder',col)
% 
% [hfg,hax,hpl]=fpplot('xpl',repmat({p},[numel(b)+2,1]),'ypl',...
%     mat2cell([F;F(1,:);F(end,:)],ones(numel(b)+2,1),numel(p)),...
%     'xlm',{[0,1]},'xtk',{0:0.1:1},'xlb',{'p''/p_C [1]'},...
%     'ylm',{[1,6]},'ytk',{1:0.6:6},'ylb',{'p_c/p_{c0} [1]'},...
%     'lwd',[ones(numel(b),1);3;3],'lst',[repmat({'-'},[numel(b),1]);{'--'};{':'}],...
%     'leg',{[repmat({''},[numel(b),1]);{'$b=0$'};{'$b=1$'}]},...
%     'tit',{'F_K(p/p_C)'},'vfg','on');
% hfg.Children(1).FontSize=18;
% 
% col=[hsv(numel(beta));0,0,0;0,0,0];
% set(0,'defaultaxescolororder',col)
% [hfg,hax,hpl]=fpplot('xpl',repmat({evp},[numel(beta)+2,1]),'ypl',...
%     mat2cell([pc;pc(1,:);pc(end,:)],ones(numel(beta)+2,1),numel(evp)),...
%     'xlm',{10.^[-7,-3]},'xtk',{10.^(-7:-3)},'xlb',{'\epsilon_V^{pl} [1]'},...
%     'ylm',{[1,1.1]},'ytk',{1:0.01:1.1},'ylb',{'p_C/p_{C0} [1]'},...
%     'lwd',[ones(numel(beta),1);3;3],'lst',[repmat({'-'},[numel(beta),1]);{'--'};{':'}],...
%     'leg',{[repmat({''},[numel(beta),1]);{'$beta=0$'};{'$beta=1$'}]},...
%     'tit',{'p_C/p_{C0}(\epsilon_V^{pl})'},'scl',{'slx'},'vfg','on');
% hfg.Children(1).FontSize=18;

col=[hsv(numel(beta));0,0,0;0,0,0];
set(0,'defaultaxescolororder',col)
[hfg,hax,hpl]=fpplot('xpl',{lambda},'ypl',{r},...
    'xlm',{10.^[-6,-1]},'xtk',{10.^(-6:-1)},'xlb',{'\lambda [1]'},...
    'ylm',{[0,1]},'ytk',{0:0.1:1},'ylb',{'r [1]'},...
    'lwd',3,'tit',{'r_{dev}'},'scl',{'slx'},'vfg','on');
hfg.Children(1).FontSize=18;
