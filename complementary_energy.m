function [varargout]=complementary_energy(varargin)
    def.omega = 0;
    def.strain = zeros(6,1);
    def.dstress = zeros(6,1);
    
    inp = inputParser;
    addParameter(inp,'omega'  ,def.omega  ,@isnumeric);    
    addParameter(inp,'strain' ,def.strain ,@isnumeric);
    addParameter(inp,'dstress',def.dstress,@isnumeric);

    parse(inp,varargin{:});
    nout=0;
    
    if ~strcmpi(inp.UsingDefaults,'dstress')
        strain  = inp.Results.strain;
        dstress = inp.Results.dstress;
        nout=nout+1;
        varargout{nout} = strain(:).'*dstress(:);
    end
    
    if ~strcmpi(inp.UsingDefaults,'omega')
        strain  = inp.Results.strain;
        dstress = inp.Results.dstress;
        omega   = inp.Results.omega+strain(:).'*dstress(:);
        nout=nout+1;
        varargout{nout} = omega;
    end
    
    return
end