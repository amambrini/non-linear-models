function [varargout]=strain_energy(varargin)
    def.w = 0;
    def.dstrain = zeros(6,1);
    def.stress = zeros(6,1);
    
    inp = inputParser;
    addParameter(inp,'w'      ,def.w  ,@isnumeric);    
    addParameter(inp,'dstrain',def.dstrain ,@isnumeric);
    addParameter(inp,'stress' ,def.stress,@isnumeric);

    parse(inp,varargin{:});
    nout=0;
    
    if ~strcmpi(inp.UsingDefaults,'stress')
        dstrain = inp.Results.dstrain;
        stress = inp.Results.stress;
        nout=nout+1;
        varargout{nout} = dstrain(:).'*stress(:);
    end
    
    if ~strcmpi(inp.UsingDefaults,'w')
        dstrain  = inp.Results.dstrain;
        stress = inp.Results.stress;
        w   = inp.Results.w+dstrain(:).'*stress(:);
        nout=nout+1;
        varargout{nout} = w;
    end
    
    return
end