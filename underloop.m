function F = underloop(x)
    global Gmax taub taup taudu gamma0
    F(1) = taudu-taup-x(1)/(x(2)+1);
    F(2) = Gmax-x(1)*log(10)*(x(3)-100)/2/(x(2)+1)^2;
    F(3) = taub-...
        (taup+x(1)/(x(2)+1)-x(1)/2*(1/(x(2)+10^(200*gamma0))+...
        1/(x(2)+10^(-2*gamma0*x(3)))));