%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% HypoElastic Chitas Chen model %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAIN ACTORS:
% eps: collects all the vectors epsilon (strains) for every given increment 
% sig: collects all the vectors sigma (stresses) for every given increment
% e: vector epsilon (strains) of the previous increment
% de: increment vector epsilon (strains) 
% sa:vector sigma (stresses) of the previous increment
% dsa: increment vector sigma (stresses)
% gamma_oct_true: gamma octahedral
% gamma_oct_sign: gamma octahedral with a sign
% tau_oct_true: tau octahedral
% tau_oct_sign: tau octahedral with a sign
ccc;
% CALIBRATE ACCURATELY tol_eps AND tol_sig
%strain = importdata('TS_sinus_1.csv');
%strain = rmfield(strain,{'textdata','colheaders'});
fc = 1.0;
Tc = 3.0*fc;
sampl = 0.001;
vtm = 0:sampl:Tc;
% DATA SETS
% strain.data =[vtm.',sin(2.0*fc*pi*vtm.')+1.0/2.0*sin(2.0*2.0*fc*pi*vtm.')+1.0/4.0*sin(2.0*4.0*fc*pi*vtm.')+1.0/8.0*sin(2.0*8.0*fc*pi*vtm.')+1.0/16.0*sin(2.0*16.0*fc*pi*vtm.')+1.0/32.0*sin(2.0*32.0*fc*pi*vtm.')+1.0/64.0*sin(2.0*64.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.')];
strain.data =[vtm.',sin(2.0*fc*pi*vtm.')+1.0/2.0*sin(2.0*2.0*fc*pi*vtm.')+1.0/4.0*sin(2.0*4.0*fc*pi*vtm.')+1.0/8.0*sin(2.0*8.0*fc*pi*vtm.')+1.0/16.0*sin(2.0*16.0*fc*pi*vtm.')+1.0/32.0*sin(2.0*32.0*fc*pi*vtm.')+1.0/64.0*sin(2.0*64.0*fc*pi*vtm.')+1.0/16.0*sin(2.0*64.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.')];
% strain.data =[vtm.',sin(2.0*fc*pi*vtm.')+1.0/2.0*sin(2.0*2.0*fc*pi*vtm.')+1.0/4.0*sin(2.0*4.0*fc*pi*vtm.')+1.0/8.0*sin(2.0*8.0*fc*pi*vtm.')+1.0/16.0*sin(2.0*16.0*fc*pi*vtm.')+1.0/32.0*sin(2.0*32.0*fc*pi*vtm.')+1.0/64.0*sin(2.0*64.0*fc*pi*vtm.')+1.0/256.0*sin(2.0*256.0*fc*pi*vtm.')+1.0/1024.0*sin(2.0*1024.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.')];
%
strain.maxa = 1e-3;
strain.data(:,2:end) = strain.maxa.*...
    strain.data(:,2:end)./max(abs(strain.data(:,2:end)),[],1);
%
strain.ntm = size(strain.data,1);
%
mu0 = 1700*300^2;
nu  = 1.0/3.0;
K   = 2.0*mu0.*(1.0+nu)./3.0./(1.0-2.0.*nu);
%
tensor_invariants;
lp=1;
eps = [zeros(strain.ntm,5),strain.data(:,lp+1)];
sig = zeros(strain.ntm,6);
%
% CALIBRATE ACCURATELY THESE PARAMETERS!
tol_sig = 1.3e03; % define a 'zero' for stress
tol_eps = 1.0e-06; % define a 'zero' for gamma oct: in order to do that,
% analyse the 'true' gamma_oct and check the order of
% 'zero'. This is FUNDAMENTAL, in order to have a
% correct performance of the program
% I MAY SUBSTITUTE THIS TOLERANCE WITH A UNIQUE TOLERANCE IN TERMS OF
% TANGENT! WHEN A DEVIATION APPEARS, I CAN CHANGE SIGN!!!!!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% trend of gamma oct: find all the signs and the inversions, defined as:
% >>> sign: + or -
% >>> inversion: loading/unloading/reloading
gamma_oct_true = zeros(strain.ntm,1);
gamma_oct_sign = zeros(strain.ntm,1);
tau_oct_sign = zeros(strain.ntm,1);
trend_inv = zeros(strain.ntm,1);
which_curve = zeros(strain.ntm,1);
flag.epsign = 0;
% gamma octahedral (true: without the sign)
for i_=2:strain.ntm
    gamma_oct_true(i_) = tns.oct(eps(i_,:)); % true gamma oct (semiposit.)
end
flag.epsup = 1;
% start recovering the sign, by 'creating' a meaningful gamma_oct_sign,
% which is capable to include not only the measure of the cycle, but also
% the sign of it
for i_=2:strain.ntm
    if i_ == 91 % 410 % test line
        % keyboard
    end
    if sum(gamma_oct_true(i_) >= gamma_oct_true(1:i_)) >= i_
        which_curve(i_) = 1; % loading
    else
        which_curve(i_) = 0; % unloading/reloading
    end
    if i_ == 2 % use just to start the cycle
        flag.epsign = 0;
        gamma_oct_sign(i_) = gamma_oct_true(i_);
        % when the value of gamma is close to zero, I have to check if
        % there's a change in the sign of it
    elseif   gamma_oct_true(i_-1) < gamma_oct_true(i_) && ...
            abs(gamma_oct_sign(i_-1)) <= tol_eps || abs(gamma_oct_true(i_))
        if  flag.epsup == 0 && flag.epsign == 0
            % compare the tangents
            d_comp = (gamma_oct_sign(i_-2)-gamma_oct_sign(i_-1))/...
                ((i_-2)-(i_-1));
            d_plus = (gamma_oct_sign(i_-1)-gamma_oct_true(i_))/...
                ((i_-1)-(i_));
            d_minus = (gamma_oct_sign(i_-1)-(-gamma_oct_true(i_)))/...
                ((i_-1)-(i_));
            d_complus = abs((d_comp-d_plus)/d_comp);
            d_compmin = abs((d_comp-d_minus)/d_comp);
            d_ok = min(d_complus,d_compmin);
            if d_ok == d_complus % && d_plus < 0
                gamma_oct_sign(i_) = gamma_oct_true(i_);
                flag.epssign = 0;
            else
                gamma_oct_sign(i_) = -gamma_oct_true(i_);
                flag.epsign = 1;
            end
        elseif flag.epsup == 1 && flag.epsign == 1 && ...
                abs(gamma_oct_sign(i_-1)) <= tol_eps || abs(gamma_oct_true(i_))
            % compare the tangents
            d_comp = (gamma_oct_sign(i_-2)-gamma_oct_sign(i_-1))/...
                ((i_-2)-(i_-1));
            d_plus = (gamma_oct_sign(i_-1)-gamma_oct_true(i_))/...
                ((i_-1)-(i_));
            d_minus = (gamma_oct_sign(i_-1)-(-gamma_oct_true(i_)))/...
                ((i_-1)-(i_));
            d_complus = abs((d_comp-d_plus)/d_comp);
            d_compmin = abs((d_comp-d_minus)/d_comp);
            d_ok = min(d_complus,d_compmin);
            if d_ok == d_complus % || d_minus < 0
                gamma_oct_sign(i_) = gamma_oct_true(i_);
                flag.epssign = 0;
            else
                gamma_oct_sign(i_) = -gamma_oct_true(i_);
                flag.epsign = 1;
            end
        elseif flag.epsign == 1
            gamma_oct_sign(i_) = -gamma_oct_true(i_);
        elseif flag.epsign == 0
            gamma_oct_sign(i_) = gamma_oct_true(i_);
        else
            disp('ERROR in evaluating sign of gamma w/ small tau');
            keyboard
        end
    elseif flag.epsign == 1
        gamma_oct_sign(i_) = -gamma_oct_true(i_);
    elseif flag.epsign == 0
        gamma_oct_sign(i_) = gamma_oct_true(i_);
    else
        disp('ERROR in evaluating sign of gamma');
        keyboard
    end
    if gamma_oct_sign(i_) > gamma_oct_sign(i_-1)
        flag.epsup = 1; % increasing gamma
    else
        flag.epsup = 0; % decreasing gamma
    end
end
count_inv = 0;
% spot and count the real inversions of gamma
for i_=2:strain.ntm-1
    if ((gamma_oct_sign(i_) > gamma_oct_sign(i_-1) && ...
            gamma_oct_sign(i_) > gamma_oct_sign(i_+1)) || ...
            (gamma_oct_sign(i_) < gamma_oct_sign(i_-1) && ...
            gamma_oct_sign(i_) < gamma_oct_sign(i_+1)) && ...
            (gamma_oct_sign(i_-1) > 0 && gamma_oct_sign(i_+1) > 0) ) || ... % min/max for positive gammas
            ((gamma_oct_sign(i_) > gamma_oct_sign(i_-1) && ...
            gamma_oct_sign(i_) > gamma_oct_sign(i_+1)) || ...
            (gamma_oct_sign(i_) < gamma_oct_sign(i_-1) && ...
            gamma_oct_sign(i_) < gamma_oct_sign(i_+1)) && ...
            (gamma_oct_sign(i_-1) < 0 && gamma_oct_sign(i_+1) < 0) ) || ... % min/max for negative gammas
            (gamma_oct_sign(i_) < gamma_oct_sign(i_-1) && ...
            gamma_oct_sign(i_) < gamma_oct_sign(i_+1)) || ...
            (gamma_oct_sign(i_) > gamma_oct_sign(i_-1) && ...
            gamma_oct_sign(i_) > gamma_oct_sign(i_+1))
        trend_inv(i_) = 1;
        count_inv = count_inv + 1;
        gamma_inv_all(count_inv) = gamma_oct_sign(i_);
    end
end
% plot the results in terms of gamma
figure('pos',[265 120 900 600])
subplot(4,1,1)
plot(strain.data(:,1),strain.data(:,2),'b'); hold all
title('input');
xlabel('t');
ylabel('\gamma_{yz}');
subplot(4,1,2)
plot(1:strain.ntm,gamma_oct_true,'b'); hold all
title('octahedral gamma');
xlabel('step');
ylabel('\gamma_{oct}');
subplot(4,1,3)
plot(1:strain.ntm,trend_inv,'ro'); hold all
title('inversions spotted');
xlabel('step');
ylabel('yes (=1) or no (=0)');
subplot(4,1,4)
plot(1:strain.ntm,gamma_oct_sign,'b'); hold all
title('extended octahedral gamma, with a sign');
xlabel('step');
ylabel('\gamma_{oct, extended}');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialization for the tau part
sa   = zeros(1,6);
ebb  = zeros(1,6);
e_rel= zeros(1,6);
sa_inv = zeros(1,6);
sbb  = zeros(1,6);
tau_oct_true = zeros(strain.ntm,1);
gamma_oct = zeros(strain.ntm,1);
tau_oct = zeros(strain.ntm,1);
gamma_oct_max = 0;
%
gamma_ref = 1e-4;
tau_ult = gamma_ref*mu0;
%
lcf = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% start the program analysis
flag.Iq = 1;
flag.IIq = 0;
flag.IIIq = 0;
flag.IVq = 0;
flag.endloop = 0;
flag.up = 1;
level = 0;
n_inv = 0;
%
i_=1;
fprintf('step %d: LOADING input \n',i_);
% start looking for the values of tau_oct. Here as well we are defining two
% different taus: the real one (w/out sign) and a modified one, including
% the sign, in order to properly reconstruct the loop in terms of
% gamma_oct_sign and tau_oct_sign
for i_=2:strain.ntm
    % current strain/strain increment
    e  = eps(i_-1,:);
    de = eps(i_,:)-e;
    gamma_oct(i_) = gamma_oct_sign(i_);
    prev_level = level;
    %
    if i_ ==  73 % 41 % 2857 % 780 % 373 % test line
        % keyboard
    end
    if which_curve(i_) == 1
        % e_max = e+de;
        if gamma_oct_true(i_) > gamma_oct_max && ...
                gamma_oct_true(i_)-gamma_oct_max > tol_eps*10^-3
            e_max = e+de; % loading
            gamma_oct_max = gamma_oct_true(i_);
        elseif gamma_oct_true(i_) == gamma_oct_max || ...
                gamma_oct_true(i_)-gamma_oct_max < tol_eps
            which_curve(i_) = 0; % max reached (or very close to it!)
        else
            disp('ERROR in LOADING')
            keyboard
        end
    elseif which_curve(i_) == 0 % unloading/reloading
        if level > 2 && ...
                (((gamma_oct_sign(i_) > gamma_start(level-1) && ...
                gamma_start(level-1) > gamma_start(level)) || ...
                (gamma_oct_sign(i_) < gamma_start(level-1) && ...
                gamma_start(level-1) < gamma_start(level))) || ...
                (gamma_start(level-1) < 0 && ...
                (gamma_oct_true(i_) < gamma_start(level-1) && ...
                gamma_start(level-1) < gamma_start(level))))
            % overcome the val of the prev loop: exit the loop
            flag.endloop = 1;
            level = level-2;
            e = e_start(level+1,:);
            e_loop = e_start(level,:);
            de = eps(i_,:)-e;
        elseif trend_inv(i_-1) == 1
            level = level+1;
            n_inv = n_inv+1;
            e_start(level,:) = e(:); % start a loop from the loading curve
            sa_start(level,:) = sa(:);
            gamma_start(level) = gamma_oct_sign(i_-1);
            e_loop = e_start(level,:);
        else % keep going!
        end
    end
    % backbone curve
    [Fbba,~] = HE_norm_bb_curve('mod','hyperbolic',...
        'val',tns.oct(e(:))./gamma_ref);
    [Fbb,dFbb] = HE_norm_bb_curve('mod','hyperbolic',...
        'val',tns.oct(de(:)+e(:))./gamma_ref);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % evaluation of the increments
    % in loading
    if which_curve(i_) == 1
        fprintf('step %d: LOADING \n',i_);
        dsa = HE_dstress('epsilon',e,'depsilon',de,...
            'Gs',Fbb*tau_ult/tns.oct(e(:)+de(:)),'Gt',dFbb*mu0,'K',K);
        if level > 0
            sa = sa_start(1,:)'+dsa;
            level = 0;
        else
            sa = sa(:)+dsa;
        end
        sbb=sa;
        ebb=e+de;
        flag.loading = 0;
        % in unloading/reloading
    elseif which_curve(i_) == 0
        fprintf('step %d: UNLOADING/RELOADING \n',i_);
        [phi,dphi] = HE_hysteresis_functional('mod','puzrin_burland_1996',...
            'bbmod','hyperbolic','Gmax',mu0,'gamma_bb',tns.oct(ebb),...
            'gamma_val',tns.oct(e+de-e_loop),'tau_bb',tns.oct(sbb),...
            'tau_ult',tau_ult);
        if gamma_oct_sign(i_-1) < 0 && gamma_oct_sign(i_) > 0
            ; % correction when gamma changes sign
        else
            dsa = HE_dstress('epsilon',e,'depsilon',de,'Gs',phi,'Gt',dphi,'K',K);
        end
        if flag.endloop == 1
            sa = sa_start(level+1,:)'+dsa; % correction when a subloop ends
            flag.endloop = 0;
        else
            sa = sa+dsa;
        end
    else
        disp('ERROR IN EVALUATING sa');
        keyboard
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % define the tau_oct with a sign, starting from here
    sig(i_,:) = sa(:).';
    tau_oct_true(i_) = tns.oct(sa(:)); % value of actual tau_oct_true
    if tau_oct_sign(i_-1) < 0
        flag.sigsign = 1;
    else
        flag.sigsign = 0;
    end
    % define the value of actual tau_oct_sign
    if i_ == 2 % use just to start the cycle
        flag.sigsign = 0;
        tau_oct_sign(i_) = tns.oct(sa(:));
        % if the value of tau is close to zero
    elseif  (tau_oct_true(i_-1) < tau_oct_true(i_-2) && ...
            (tau_oct_true(i_-1) <= tol_sig)||(tau_oct_true(i_) <= tol_sig))
        if  (flag.up == 0 && flag.sigsign == 0 && trend_inv(i_-1) == 0) ...
                || ( trend_inv(i_-1) == 1 && flag.up == 0 )
            % check the tangent
            m_comp = (tau_oct_sign(i_-2)-tau_oct_sign(i_-1))/...
                (gamma_oct_sign(i_-2)-gamma_oct_sign(i_-1));
            m_plus = (tau_oct_sign(i_-1)-tau_oct_true(i_))/...
                (gamma_oct_sign(i_-1)-gamma_oct_sign(i_));
            m_minus = (tau_oct_sign(i_-1)-(-tau_oct_true(i_)))/...
                (gamma_oct_sign(i_-1)-gamma_oct_sign(i_));
            complus = abs((m_comp-m_plus)/m_comp);
            compmin = abs((m_comp-m_minus)/m_comp);
            m_ok = min(complus,compmin);
            if m_ok == complus
                tau_oct_sign(i_) = tau_oct_true(i_);
            else
                tau_oct_sign(i_) = -tau_oct_true(i_);
                flag.sigsign = 1;
            end
        elseif (flag.up == 1 && flag.sigsign == 1 && trend_inv(i_-1) == 0) ...
                || ( trend_inv(i_-1) == 1 && flag.up == 1 )
            % check the tangent
            m_comp = (tau_oct_sign(i_-2)-tau_oct_sign(i_-1))/...
                (gamma_oct_sign(i_-2)-gamma_oct_sign(i_-1));
            m_plus = (tau_oct_sign(i_-1)-tau_oct_true(i_))/...
                (gamma_oct_sign(i_-1)-gamma_oct_sign(i_));
            m_minus = (tau_oct_sign(i_-1)-(-tau_oct_true(i_)))/...
                (gamma_oct_sign(i_-1)-gamma_oct_sign(i_));
            complus = abs((m_comp-m_plus)/m_comp);
            compmin = abs((m_comp-m_minus)/m_comp);
            m_ok = min(complus,compmin);
            if m_ok == complus
                tau_oct_sign(i_) = tau_oct_true(i_);
                flag.sigsign = 0;
            else
                tau_oct_sign(i_) = -tau_oct_true(i_);
            end
        elseif flag.sigsign == 1
            tau_oct_sign(i_) = -tau_oct_true(i_);
            % flag.endloop2 = 0;
        elseif flag.sigsign == 0
            tau_oct_sign(i_) = tau_oct_true(i_);
            % flag.endloop2 = 0;
        else
            disp('ERROR in evaluating the change in sign of gamma');
            keyboard
        end
    elseif (flag.sigsign == 1 && ((flag.up == 0 && level == 1)|| ...
            (flag.up == 0 && tau_oct_sign(i_-1) < 0))) || ...
            (flag.sigsign == 1 && flag.up == 0 && level > 1)
        tau_oct_sign(i_) = -tns.oct(sa(:));
        flag.sigsign = 1;
    elseif flag.sigsign == 0 % || prev_level == 1
        tau_oct_sign(i_) = tns.oct(sa(:));
        flag.sigsign = 0;
    elseif flag.sigsign == 1 % || prev_level == 1
        tau_oct_sign(i_) = -tns.oct(sa(:));
        flag.sigsign = 1;
    elseif flag.up == 1 && tau_oct_sign(i_-1) < 0 && trend_inv(i_-1) == 0
        if -tau_oct_true(i_) > tau_oct_sign(i_-1)
            tau_oct_sign(i_) = sign(tau_oct_sign(i_-1))*tau_oct_true(i_);
        else
            tau_oct_sign(i_) = tau_oct_true(i_);
            flag.sigsign = 0;
        end
    else
        disp('ERROR in evaluating sign of tau');
        keyboard
    end
    %
    if tau_oct_sign(i_) > tau_oct_sign(i_-1)
        flag.up = 1; % tau increasing
    elseif tau_oct_sign(i_) < tau_oct_sign(i_-1)
        flag.up = 0; % tau decreasing
    else
        disp('ERROR UP OR DOWN')
    end
end
% final plots
figure('pos',[265 120 900 600])
subplot(2,1,1)
plot(1:strain.ntm,tau_oct_true,'b'); hold all
title('octahedral tau');
xlabel('step');
ylabel('\tau_{oct}');
subplot(2,1,2)
plot(1:strain.ntm,tau_oct_sign,'b'); hold all
title('extended octahedral tau, with a sign');
xlabel('step');
ylabel('\tau_{oct, extended}');
%
figure('pos',[265 120 900 600])
plot(gamma_oct_sign(1:i_-1),tau_oct_sign(1:i_-1),'b'); hold all; grid on
% plot(gamma_oct_sign(i_-1),tau_oct_sign(i_-1),'rx'); hold all; grid on
% axis([]);
title('Chitas 3D extension of the Ramberg-Osgood model');
xlabel('\gamma_{oct, extended}');
ylabel('\tau_{oct, extended}');
% figure
% plot(gamma_oct,tau_oct./1e6,'b');
% xlabel('\gamma _{oct} [\]');
% ylabel('\tau _{oct} [MPa]');
%
% figure
% plot(eps(:,end),sig(:,end)./1e6,'b');
% xlabel('\gamma [\]');
% ylabel('\tau [MPa]');

% format_figures; rule_fig(gcf);

% figure(2)
% plot(1:i_,rad) % inserito 'a mano' quello attuale
% title('radius')
%
% figure(3)
% plot(1:i_,ang) % inserito 'a mano' quello attuale
% title('angle')

% figure
% semilogx(eps(:,end),sig(:,end)./eps(:,end)./mu0,'b');
% xlabel('\gamma [1]');
% ylabel('\mu/\mu_0 [1]');
% format_figures; rule_fig(gcf);
