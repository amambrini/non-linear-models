ccc;
%
strain = importdata('TS_sinus_1.csv');
strain = rmfield(strain,{'textdata','colheaders'});
% fc = 1.0;
% Tc = 3.0*fc;
% vtm = 0:0.001:Tc;
% strain.data =[vtm.',sin(2.0*fc*pi*vtm.')+1.0/4.0*sin(2.0*4.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.'),sin(2.0*fc*pi*vtm.')];

% strain.data = [(1:1001)',(0:1e-6:1e-3)',(0:1e-6:1e-3)',(0:1e-6:1e-3)'];
strain.maxa = 1e-2;
strain.data(:,2:end) = strain.maxa.*...
    strain.data(:,2:end)./max(abs(strain.data(:,2:end)),[],1);

strain.ntm = size(strain.data,1);
%
% figure
% plot(strain.data(:,1),strain.data(:,2),'b');
% hold all
% plot(strain.data(:,1),strain.data(:,3),'r');
% plot(strain.data(:,1),strain.data(:,4),'color',rgb('intensegreen'));
% leg=legend('ACC','VEL','DIS');
% set(leg,'box','off','location','northwest');
% xlabel('t [s]');
% ylabel('\gamma [1]');
% format_figures; rule_fig(gcf);

b1 = -5.0e-6;
a1 = b1*4;
a2 = 0.0;
b0 = 0.0;
a0 = b0;
b2 = 0.0;
mu0 = 2000*500^2;
nu  = 0.3;
K   = 2.0*mu0.*(1.0+nu)./3.0./(1.0-2.0.*nu);
%
HE_basics_functions;
%
bbc.eps = 1e-8:1e-6:1e-2;
bbc.val(1) = 0;
for i_=2:numel(bbc.eps)
    bbc.val(i_)=bbc.val(i_-1)+...
        (bbc.eps(i_)-bbc.eps(i_-1))*...
        mul(mu0,[0,0,0,0,0,bbc.eps(i_-1)],[0,0,0,0,0,bbc.val(i_-1)]);
end
%
lp = 1;
eps = [zeros(strain.ntm,5),strain.data(:,lp+1)];
sig = zeros(strain.ntm,6);
rev = zeros(strain.ntm,6);
sa  = zeros(1,6);
ec = zeros(6,1);
sc = zeros(6,1);

C = 1.00;
gr = 1e-4;
tu = gr*mu0;
omegam = 0;
figure
lcf='L';
sgg=0;

for i_=1:strain.ntm-1
    e  = eps(i_,:);
    de = eps(i_+1,:)-e;
    
    dst = mu0.*tns.dv(de)+tns.I1(de).*K*tns.mv;
    [lcf,~]=URL_conditions(e,sa,dst,omegam,lcf);
    
    
    switch lcf
        case 'U'
        sc=sa;
        ec=e;
        
    end
    
    C = 1.0-sgg(i_+1)*tns.oct(sc)./tu;
    
    sa = sc(:)+2.0*(mu0./(1.0+tns.oct(e(:)+de(:)-ec(:))./(C*gr))).*...
        tns.Am1*tns.dv(de(:)+e(:)-ec(:))+...
        tns.I1(e(:)+de(:)).*K*tns.mv;
        
    [omega(i_+1),~] = ...
        complementary_energy('strain',e+de,'stress',sa);
    [~,domega(i_)] = ...
        complementary_energy('strain',e,'stress',sig(i_,:),...
        'dstress',sa(:).'-sig(i_,:));
    if omega(i_+1)>omegam
        omegam=omega(i_+1);
    end
    
    sig(i_+1,:) = sa(:).';
    
end
figure
plot(eps(:,end),sig(:,end)./1e6,'b');
xlabel('\gamma [1]');
ylabel('\tau [MPa]');
format_figures; rule_fig(gcf);

% figure
% semilogx(eps(:,end),sig(:,end)./eps(:,end)./mu0,'b');
% xlabel('\gamma [1]');
% ylabel('\mu/\mu_0 [1]');
% format_figures; rule_fig(gcf);
