function [varargout] = chen_skeleton_curve(varargin)
    %
    tau.uu=varargin{1};
    tau.du=varargin{2};
    mu.max=varargin{3};
    %
    gamma = varargin{4};
    %
    tau.val = tau.du-tau.du.*(tau.uu-tau.du)./2./tau.uu.*...
        (1.0./(-tau.du./tau.uu+10.0.^(-100.0.*gamma)));
    %
    varargout{1}=tau;
    return
end